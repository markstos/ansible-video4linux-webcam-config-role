video4linux-webcam-config
=========

This Ansible role sets up a Linux udev rule and systemd service to persist Video4Linux webcam configs. The defaults set a fixed focus on a Logitech C920 because autofocus does not work well on Linux on that model.

However, variables can be set change the webcam model and configuration values.

Requirements
------------

You'll need to install Ansible to use this.

Installation and Use
--------------------

Generally, you have to clone this repo with Git or otherwise install the Ansible role. Then customize the configs, then run a command to install everything.

 1. `git clone https://gitlab.com/markstos/ansible-video4linux-webcam-config-role`
 2. ...edit webcam model and details in video4linux-webcam-configs/defaults/main.yml`
 3. In the directory above `video4linux-webcam-config`, run the following:

```
# Use the full-path where your repo was checked out
ansible localhost --become -m include_role --args name=$HOME/git/ansible-video4linux-webcam-config-role
 ```

Role Variables
--------------

Default variables, found in `defaults/main.yml`:

```
# Vendor ID found with `lsusb`. Defaults to Logitech
idVendor: "046d"
# Model ID found with `lsusb`. Defaults to the C920
idProduct: "0892"

# The webcam could be out of focus for this long before it's corrected.
# Smaller values cause higher battery drain due to more frequent wake ups
restartSecs: 30

# Defaults to setting a fixed focus. See `v4l2-ctl --list-ctrls` for what your camera supports
webcamConfigs:
 - "focus_automatic_continuous=0"
 - "focus_absolute=0
```
Example Playbook
----------------

If you define your workstation with an Ansible playbook, you can  incorporate this role:

    - hosts: localhost
      roles:
         - { role: username.rolename, idVendor: "XXX", idProduct": "YYY".... }

License
-------

BSD

Author Information
------------------

Mark Stosberg <mark@stosberg.com>
